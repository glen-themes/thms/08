![Screenshot preview of the theme "Kami no Himegoto" by glenthemes](https://64.media.tumblr.com/268f8562f87d587fc90b56c34814e6bb/c2504811d454c8f6-20/s640x960/aa74007e416b342b7eee80aa1273abe03ed48b01.gif)

**Theme no.:** 08  
**Theme name:** Kami no Himegoto  
**Theme type:** Free / Tumblr use  
**Description:** This theme was previously called “Malevolence”, but is now revamped as “Kami no Himegoto” and features Yato from Noragami.  
**Author:** @&hairsp;glenthemes  

**Release date:** [2015-09-21](https://64.media.tumblr.com/e9dbfb44c9acf3c43ceda40183263bfe/tumblr_ntw34nZrbr1ubolzro1_1280.png)  
**Rework date:** [2021-07-25](https://64.media.tumblr.com/19004a35e05775b34f67978071c74ef5/tumblr_ntw34nZrbr1ubolzro3_r1_540.gif)  
**Last updated:** 2023-05-06

**Post:** [glenthemes.tumblr.com/post/153890914149](https://glenthemes.tumblr.com/post/153890914149)  
**Preview:** [glenthpvs.tumblr.com/kami-no-himegoto](https://glenthpvs.tumblr.com/kami-no-himegoto)  
**Download:** [pastebin.com/HFhU5wRW](https://pastebin.com/HFhU5wRW)  
**Credits:** [glencredits.tumblr.com/kami-no-himegoto](https://glencredits.tumblr.com/kami-no-himegoto)
