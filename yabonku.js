/* which is longer, my jquery or my whole theme code? who fckin knows */

/* last updated: 2021-07-24 4:13PM GMT+8 */

// load 'pushpin' svg
var punaise = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-map-pin'><path d='M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z'></path><circle cx='12' cy='10' r='3'></circle></svg>";
  
    document.documentElement.style.setProperty('--pushpin','url("' + punaise + '")');
    
// load 'download audio' svg
    
    var dong = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-hash'><path d='M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4'></path><polyline points='7 10 12 15 17 10'></polyline><line x1='12' y1='15' x2='12' y2='3'></line></svg>";
    
    document.documentElement.style.setProperty('--dongload','url("' + dong + '")');
    
    // load hashtag SVG
    
    var hash = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-hash'><line x1='4' y1='9' x2='20' y2='9'></line><line x1='4' y1='15' x2='20' y2='15'></line><line x1='10' y1='3' x2='8' y2='21'></line><line x1='16' y1='3' x2='14' y2='21'></line></svg>";
    
    document.documentElement.style.setProperty('--Hashtag','url("' + hash + '")');
    
    // load 'reblog' and 'like' SVGs
    
    var rb = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512' xml:space='preserve'><g><g><path d='M233.204,414.094l-72.301-72.301c-5.857-5.858-15.355-5.858-21.213,0c-5.858,5.858-5.858,15.355,0,21.213l46.694,46.693H30V167.634c0-36.025,29.309-65.334,65.334-65.334h97.672c8.284,0,15-6.716,15-15s-6.716-15-15-15H95.334C42.767,72.3,0,115.066,0,167.634V424.7c0,8.284,6.716,15,15,15h171.384l-46.694,46.693c-5.858,5.858-5.858,15.355,0,21.213c2.929,2.929,6.767,4.394,10.607,4.394c3.838,0,7.678-1.465,10.606-4.393l72.301-72.3c2.813-2.813,4.394-6.628,4.394-10.606C237.598,420.723,236.017,416.908,233.204,414.094z'></path></g></g><g><g><path d='M497,72.3H325.619l46.694-46.693c5.858-5.858,5.858-15.355,0-21.213c-5.858-5.858-15.355-5.857-21.213,0l-72.3,72.298c-2.813,2.813-4.393,6.628-4.393,10.607c0,3.978,1.58,7.793,4.393,10.606l72.301,72.302c2.929,2.929,6.767,4.394,10.606,4.394c3.838-0.001,7.678-1.465,10.606-4.394c5.858-5.857,5.858-15.355,0-21.213L325.62,102.3H482v242.067c0,36.025-29.309,65.333-65.334,65.333h-97.668c-8.284,0-15,6.716-15,15s6.716,15,15,15h97.668c52.567,0,95.334-42.766,95.334-95.333V87.3C512,79.015,505.284,72.3,497,72.3z'></path></g></g></svg>";
    
    document.documentElement.style.setProperty('--Reblog-Icon','url("' + rb + '")');
    
    var like = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512' xml:space='preserve'><g><path d='M511.812,167.165c-1.702-36.088-16.232-70.306-40.914-96.353c-25.567-26.981-59.251-41.993-94.847-42.272    c-34.009-0.257-63.948,12.15-88.992,36.905c-14.257,14.091-24.699,30.11-32.057,44.104c-7.358-13.994-17.8-30.013-32.057-44.104    c-25.047-24.754-55.004-37.158-88.993-36.905c-35.545,0.278-68.917,15.336-93.967,42.4C16.029,96.82,1.896,130.993,0.19,167.165    c-4.464,94.635,70.036,158.12,182.806,254.216c19.824,16.893,40.324,34.362,62.129,53.441l9.877,8.643l9.877-8.643    c23.121-20.23,44.813-38.641,65.789-56.445C442.411,323.538,516.232,260.884,511.812,167.165z M439.818,275.475    c-29.97,36.353-73.478,73.279-128.562,120.03c-18.057,15.325-36.642,31.099-56.253,48.129    c-18.29-15.892-35.664-30.698-52.55-45.087c-55.77-47.524-99.819-85.061-130.02-121.835    c-30.986-37.731-44.024-71.079-42.276-108.133c2.89-61.271,48.585-109.605,104.029-110.039c0.271-0.002,0.533-0.003,0.803-0.003    c57.965,0,87.525,49.314,100.005,78.802c3.424,8.09,11.278,13.318,20.007,13.318c8.729,0,16.583-5.228,20.007-13.318    c12.48-29.49,42.033-78.801,100.004-78.801c0.267,0,0.535,0.001,0.803,0.003c55.624,0.435,103.188,49.799,106.029,110.039    C483.57,205.158,470.609,238.125,439.818,275.475z'></path></g></svg>";
    
    document.documentElement.style.setProperty('--Heart-Icon','url("' + like + '")');
    
    // load 'previous page' svg
    
    var prev = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-hash'><polyline points='11 17 6 12 11 7'></polyline><polyline points='18 17 13 12 18 7'></polyline></svg>";
    
    document.documentElement.style.setProperty('--BackSVG','url("' + prev + '")');
    
    // load 'next page' svg
    
    var next = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-hash'><polyline points='13 17 18 12 13 7'></polyline><polyline points='6 17 11 12 6 7'></polyline></svg>";
    
    document.documentElement.style.setProperty('--NextSVG','url("' + next + '")');
    
    // load glen svg
     var couillon = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='150pt' height='150pt' viewBox='0 0 150 150' preserveAspectRatio='xMidYMid meet'><g transform='translate(0,150) scale(0.1,-0.1)' fill='black' stroke='none'><path d='M635 1320 c-110 -44 -267 -105 -348 -137 -82 -32 -160 -69 -173 -82 l-24 -24 0 -358 c0 -357 0 -358 23 -383 15 -18 103 -58 287 -131 146 -58 272 -105 280 -105 27 0 534 202 554 221 18 17 19 35 17 382 -2 291 0 365 11 373 7 6 36 19 63 29 28 10 75 29 105 42 l55 24 -34 15 c-18 8 -45 14 -59 14 -14 0 -181 -61 -369 -136 l-344 -135 -52 19 c-275 104 -369 145 -353 154 34 19 571 228 586 228 9 0 78 -25 155 -55 147 -58 172 -62 220 -41 l30 14 -35 15 c-122 53 -350 137 -371 137 -13 -1 -114 -36 -224 -80z m-210 -365 c132 -52 249 -95 260 -95 11 0 123 41 248 90 126 50 232 90 238 90 5 0 9 -134 9 -338 l0 -338 -221 -87 c-122 -48 -225 -84 -230 -81 -5 3 -9 81 -9 173 0 147 -2 171 -17 189 -10 10 -66 36 -125 56 l-106 38 -43 -19 -43 -19 121 -43 c67 -24 124 -50 127 -58 3 -8 6 -84 6 -169 0 -117 -3 -154 -12 -154 -7 0 -112 41 -233 89 l-220 89 -3 341 c-1 188 1 341 5 341 5 0 116 -43 248 -95z'></path></g></svg>";
    
    document.documentElement.style.setProperty('--glenSVG','url("' + couillon + '")');
    
    // load paint smear svg
    var sstueotr = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.0' width='301.000000pt' height='39.000000pt' viewBox='0 0 301.000000 39.000000' preserveAspectRatio='xMidYMid meet'> <g transform='translate(0.000000,39.000000) scale(0.100000,-0.100000)' fill='black' stroke='none'> <path d='M525 298 c-93 -4 -215 -12 -270 -16 l-100 -9 145 -3 145 -3 -95 -11 -95 -10 185 -9 c102 -5 218 -9 258 -9 39 0 72 -4 72 -8 0 -5 15 -12 33 -16 19 -4 26 -9 17 -12 -8 -3 -17 -8 -20 -12 -3 -4 -84 -10 -180 -14 -225 -8 -284 -19 -151 -29 52 -4 131 -7 177 -7 46 0 86 -4 89 -10 3 -5 -2 -10 -11 -10 -30 0 -11 -7 39 -14 29 -5 47 -3 47 3 0 5 -10 11 -22 14 -13 2 36 4 107 5 72 1 137 -3 145 -8 11 -7 3 -10 -27 -10 -23 0 -45 -5 -48 -10 -8 -13 145 -13 180 0 15 6 32 7 38 3 7 -4 4 -9 -8 -13 -45 -16 832 -44 1095 -36 267 9 523 27 538 39 25 19 -8 34 -91 41 -45 4 -102 10 -127 14 l-45 7 45 6 c39 5 34 6 -35 9 l-80 2 50 10 c27 5 55 14 62 20 12 10 79 17 188 19 89 1 111 7 112 29 2 19 -5 20 -95 20 -138 0 -878 19 -997 25 -425 22 -936 28 -1270 13z m530 -117 c32 -7 23 -9 -50 -9 -61 0 -80 3 -60 8 40 10 63 10 110 1z'/> </g> </svg>";

    document.documentElement.style.setProperty('--paintSmear','url("' + sstueotr + '")');
    
    // load music svg
    // src: flaticon.com/free-icon/musical-note-symbol_44871
    var clfctwh = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Capa_1' x='0px' y='0px' width='38.178px' height='38.178px' viewBox='0 0 38.178 38.178' style='enable-background:new 0 0 38.178 38.178;' xml:space='preserve'> <g> <path d='M34.054,17.222c-0.211,0.189-0.522,0.199-0.747,0.028l-7.443-5.664l-3.526,21.095c-0.013,0.08-0.042,0.153-0.083,0.219 c-0.707,3.024-4.566,5.278-9.104,5.278c-5.087,0-9.226-2.817-9.226-6.28s4.138-6.281,9.226-6.281c2.089,0,4.075,0.467,5.689,1.324 l4.664-26.453c0.042-0.241,0.231-0.434,0.476-0.479c0.236-0.041,0.484,0.067,0.61,0.28L34.17,16.48 C34.315,16.726,34.266,17.033,34.054,17.222z'/> </g> </svg>";

document.documentElement.style.setProperty('--nota','url("' + clfctwh + '")');

    // load thin play
    // src: flaticon.com/free-icon/play_748134
    var uzhszvdj = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Capa_1' x='0px' y='0px' viewBox='0 0 511.999 511.999' style='enable-background:new 0 0 511.999 511.999;' xml:space='preserve'> <g> <g> <path d='M443.86,196.919L141.46,10.514C119.582-2.955,93.131-3.515,70.702,9.016c-22.429,12.529-35.819,35.35-35.819,61.041 v371.112c0,38.846,31.3,70.619,69.77,70.829c0.105,0,0.21,0.001,0.313,0.001c12.022-0.001,24.55-3.769,36.251-10.909 c9.413-5.743,12.388-18.029,6.645-27.441c-5.743-9.414-18.031-12.388-27.441-6.645c-5.473,3.338-10.818,5.065-15.553,5.064 c-14.515-0.079-30.056-12.513-30.056-30.898V70.058c0-11.021,5.744-20.808,15.364-26.183c9.621-5.375,20.966-5.135,30.339,0.636 l302.401,186.405c9.089,5.596,14.29,14.927,14.268,25.601c-0.022,10.673-5.261,19.983-14.4,25.56L204.147,415.945 c-9.404,5.758-12.36,18.049-6.602,27.452c5.757,9.404,18.048,12.36,27.452,6.602l218.611-133.852 c20.931-12.769,33.457-35.029,33.507-59.55C477.165,232.079,464.729,209.767,443.86,196.919z'/> </g> </g> </svg>";

document.documentElement.style.setProperty('--blay','url("' + uzhszvdj + '")');

    // load thin pause
    // src: flaticon.com/free-icon/pause_748136
    
    var nfuvx = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Capa_1' x='0px' y='0px' viewBox='0 0 512 512' style='enable-background:new 0 0 512 512;' xml:space='preserve'> <g> <g> <path d='M124.5,0h-35c-44.112,0-80,35.888-80,80v352c0,44.112,35.888,80,80,80h35c44.112,0,80-35.888,80-80V80 C204.5,35.888,168.612,0,124.5,0z M164.5,432c0,22.056-17.944,40-40,40h-35c-22.056,0-40-17.944-40-40V80 c0-22.056,17.944-40,40-40h35c22.056,0,40,17.944,40,40V432z'/> </g> </g> <g> <g> <path d='M482.5,352c11.046,0,20-8.954,20-20V80c0-44.112-35.888-80-80-80h-34c-44.112,0-80,35.888-80,80v352 c0,44.112,35.888,80,80,80h34c44.112,0,80-35.888,80-80c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20 c0,22.056-17.944,40-40,40h-34c-22.056,0-40-17.944-40-40V80c0-22.056,17.944-40,40-40h34c22.056,0,40,17.944,40,40v252 C462.5,343.046,471.454,352,482.5,352z'/> </g> </g> </svg>";

document.documentElement.style.setProperty('--bause','url("' + nfuvx + '")');
    
/*-------------------------------------------------*/
    
$(document).ready(function(){
    var customize_page = window.location.href.indexOf("/customize") > -1;
    var on_main = window.location.href.indexOf("/customize") < 0;
    
    // unfuck tumblr
    $(".tumblr_preview_marker___").remove();
    $(".tumblr_theme_marker_blogtitle").contents().unwrap();
    
    
    
    $(".retweet").css("background-color","var(--Post-Buttons-Color)");
    $(".heart").css("background-color","var(--Post-Buttons-Color)");
    
    setTimeout(function(){
        $(".heart").css("transition","background-color .069s ease-in-out");
    },420);
    
    // remove tumblr redirects script by magnusthemes@tumblr
    // part 1/2
    $('a[href*="t.umblr.com/redirect"]').each(function(){
      var originalURL = $(this).attr("href").split("?z=")[1].split("&t=")[0];
      var replaceURL = decodeURIComponent(originalURL);
      $(this).attr("href", replaceURL);
    });
    
    // part 2/2
    function noHrefLi(){
        var linkSet = document.querySelectorAll('a[href*="href.li/?"]');
        Array.prototype.forEach.call(linkSet,function(el,i){
            var theLink = linkSet[i].getAttribute('href').split("href.li/?")[1];
            linkSet[i].setAttribute("href",theLink);
        });
    }
    noHrefLi();
    
    
    // if:reblog dividers, increase space between 1st commenter & the next
    var p_gap = getComputedStyle(document.documentElement)
               .getPropertyValue("--Paragraph-Margins"),
        numonly = parseFloat(p_gap),
        suffix = p_gap.split(/\d/).pop();
    
    // once oui
    $("[reblog-borders*='rb-yes'] .posts p").each(function(){
        if($(this).parents(".comment_container").next().length){
            $(this).css("margin-bottom",numonly / 2 + suffix)
        }
    });
    
    // once oui
    $("[reblog-borders*='rb-yes'] .posts").each(function(){
        // remove extra space from first commenter
        // if there is nothing before it
        if(!$(this).find(".comment_container:first").closest(".body").length){
            $(this).find(".comment_container:first").addClass("ccfirst")
        
        };
        
        // remove reblog border from last commenter
        $(this).find(".comment_container:last").addClass("cclast");
    });
    
    
    $(".posts").each(function(){
        $(this).find(".comment_container:last").addClass("cclast")
    });
    
    
    // turn npf_chat into a chat post
    
    $(".npf_chat").each(function(){
        $(this).wrap("<div class='chatholder'>");
        
        // if chat post was made with old dashboard
        if($(this).is("[data-npf]")){
            
            $(this).find("b").before("<span class='chat_label'>" + $(this).find("b").text() + "</span>");
            $(this).find("b").remove();
            
            $(this).contents().last().wrap("<span class='chat_content'>")
            
        } else {
            // if chat post was made with the new editor
            if($(this).find("span").length){
                $(this).find("b").before("<span class='chat_label'>" + $(this).find("b").text() + "</span>");
                $(this).find("b").remove();
                
                $(this).find("span:not([class])").addClass("chat_content")
            }
        }
    });
    
    $(".chatholder").each(function(){
        if($(this).find(".npf_chat").length){
            $(this).find(".npf_chat").children().unwrap();
        }
    });
    
    // add colon at the end of each chat_label
    $(".chat_label").each(function(){
        if(!$(this).text().endsWith(":")){
            $(this).append(":")
        }
    });
    
    // remove duplicate colon at start of each chat_content
    $(".chat_content").each(function(){
        if($(this).text().startsWith(": ")){
            $(this).text($(this).text().slice(2))
        }
    });
    
    /*-- wrap chats in chat cont --*/
    var jqver = jQuery.fn.jquery;
    var ver = jqver.replaceAll(".","");
    var chats = $(".chatholder");
    
    if(ver < "180"){
        for(var u_u=0; u_u<chats.length;) {
           u_u += chats.eq(u_u).nextUntil(':not(.chatholder)').andSelf().wrapAll('<div class="chatcont">').length;
        }
    } else {
        for(var u_u=0; u_u<chats.length;) {
           u_u += chats.eq(u_u).nextUntil(':not(.chatholder)').addBack().wrapAll('<div class="chatcont">').length;
        }
    }
    
    $(".posts").each(function(){
        // show source if post does not have caption
        if(!$(this).find(".comment_container").length){
            $(this).find(".nosrc").css("display","flex")
        }
    });
    
    $(".nosrc").each(function(){
        if($(this).is(":hidden")){
            $(this).remove()
        }
    });
    
    $(".body").each(function(){
        var ccf = $(this).find(".comment_container:first");
        if(!$(this).prev().length){
            if(ccf.prev().is(".npf_inst")){
                ccf.css("margin-top","");
            } else {
                ccf.css("margin-top","-2px");
                if(ccf.parent().is(".body")){
                    if(ccf.children().first().is(".commenter")){
                        var cp = ccf.find(".commenter:first").css("padding-top");
                        ccf.css("margin-top","-" + cp)
                    }
                }
            }
        }
    });
    
    
    $(".has-npf .npf_inst:last").each(function(){
        if($(this).parent().prev(".commenter")){
            $(this).css("margin-top","var(--NPF-Bottom-Gap-From-Captions)")
        }
    });
    
    
    $(".has-npf").each(function(){
        
        var fc = $(this).find(".comment_container").eq(0);
        var fcinst = fc.find(".npf_inst");
        if(fcinst.length){
            if(!fcinst.prev().length){
                if(fcinst.next().length){
                    fcinst.prependTo($(this));
                    fcinst.css("margin-bottom","var(--NPF-Bottom-Gap-From-Captions)")
                    fc.css("margin-top","")
                }
            }
        }
        
        if($(this).children().first().is(".npf_inst")){
            $(this).children().first().css("margin-top","")
        }
    });
    
    $(".caption").each(function(){
        // remove empty captions
        if(!$(this).children().length){
            $(this).remove();
        }
    });
    
    /*
    $(".posts br").each(function(){
        // remove unnecessary <br>s
        if(!$(this).prev().length){
            $(this).remove();
        }
    });
    */
    
    $(".ccfirst").each(function(){
        if(!$(this).closest(".caption").length){
            if(!$(this).prev().length){
                $(this).css("margin-top","calc(var(--Captions-Gap) / -2)")
            }
        }
    });
    
    $(".comment_container:first").each(function(){
        if($(this).closest(".postinner").length){
            if($(this).prev().length == 0){
                $(this).addClass("ccfirst")
            }
        }
    });
    
    $(".commenter").each(function(){
        var getprevnpf = $(this).parent().prev();
        
        if(getprevnpf.is(".npf_inst")){
            $(this).css("padding-top","var(--NPF-Bottom-Gap-From-Captions)");
            getprevnpf.css("margin-bottom",0);
            
        }
        
        
    });
    
    // get line-height and set this as bullet points' height
    var LH = getComputedStyle(document.documentElement)
               .getPropertyValue("--Text-LineHeight"),
        LH_num = parseFloat(LH);
    
    var root = document.documentElement;
    root.style.setProperty('--LH', LH_num);
    
    $(".comment_container").each(function(){
        if($(this).prev().length == 0){
            $(this).addClass("ccfirst")
        }
    });
    
    //------- IF HEADING is first element
    $(".posts :header").each(function(){
        if($(this).prev().length == 0){
        if(!$(this).parents(".posts").find("h1.title").length){
            // turn h2 into h1
            var htmlol = $(this).prop("outerHTML");
            var h1 = htmlol.replace("<h2","<h1 class='title'").replace("</h2>","</h1>");
            $(this).replaceWith(h1);
        }
        }
    });
    
    $(".posts h1.title").each(function(){
        if($(this).closest(".npf_inst").length){
            if($(this).parent(".npf_inst").css("margin-top")){
                $(this).parent(".npf_inst").css("margin-top","");
                $(this).parent(".npf_inst").css("margin-bottom","calc(var(--NPF-Bottom-Gap-From-Captions) - 4px)");
                $(this).css("margin-bottom","calc(var(--Heading-Margins) + 2px)")
            }
        }
    });
    
    
    /*------------- CHANGE GIFV TO GIF -------------*/
    // script credit:  felicitysmoak.tumblr.com/post/188159105501
    
    $('img[src*="media.tumblr.com"]').each(function(){
         var newsrc = $(this).attr('src').replace('gifv','gif');
     $(this).attr('src', newsrc);
    });
    
    $('a[href*="media.tumblr.com"]').each(function(){
         var newhref = $(this).attr('href').replace('gifv','gif');
         $(this).attr('href', newhref);
    });
    
    $('div[data-lowres*="media.tumblr.com').each(function(){
         var newdata = $(this).attr('data-lowres').replace('gifv','gif');
         $(this).attr('data-lowres', newdata);
    });
    
    $('div[data-highres*="media.tumblr.com').each(function(){
         var newdata = $(this).attr('data-highres').replace('gifv','gif');
         $(this).attr('data-highres', newdata);
    });
    /*-----------------------------------------------*/
    
    // wrap and remove duplicate quote source
    $(".quote-source, .quote-source p").each(function(){
        $(this).contents().filter(function(){
            return this.nodeType === 3;
        }).wrap('<span/>');
        
        $(this).find(".tumblr_blog").remove();
    });
    
    $(".quote-source span").each(function(){
        var viatxt = $(this).text();
        
        if(viatxt.indexOf("(Source: ") > -1){
            $(this).remove();
        }
        
        if(viatxt.indexOf(")") > -1){
            if(viatxt.length == "1"){
                $(this).remove()
            }
        }
        
        if(viatxt.endsWith(" (via ")){
            var v = viatxt.slice(0,-6);
            $(this).text(v)
        }
    });
    
    $(".quote-source a").each(function(){
        if(!$(this).hasClass("tumblr_blog")){
            if(!$(this).siblings("span").length){
                $(this).remove();
            }
        }
    });
    
    // remove empty quote source
    $(".quote-source p").each(function(){
        if(!$(this).children().length){
            $(this).remove();
        }
    });
    
    // remove quote source whitespace
    $(".pq").each(function(){
        if($(this).prev().length == 0){
            $(this).css("margin-top","var(--Paragraph-Margins)")
        }
		
		if(!$(this).next().length){
			$(this).css("margin-bottom",0)
		}
    });
    
    // attempt to close quote-post brackets
    $(".quote-source").each(function(){
        $(this).find("span").last().each(function(){
          var qt = $(this).text();
          if(qt.endsWith("(")){
              if($(this).next("a").length){
                  $(this).next().after("<span>)</span>")
              }
          }
        })        
    });
    
    // trim last quote-source paragraph
    $(".quote-words").each(function(){
        $(this).find("p:last").each(function(){
            if(!$(this).next().length){
                $(this).css("margin-bottom",0)
            }
        })        
    });
    
    $(".has-npf").each(function(){
        var npf1st = $(this).children(".npf_inst").eq(0);
        
        if($(this).children(".npf_inst").length == 1){
            if(!npf1st.prev().length){
                npf1st.insertAfter(npf1st.parent().find("h1.title:first"));
            }
        }
    });
    
    $("h1.title").each(function(){
        if(!$(this).prev().length){
            if($(this).parent().is(".comment")){
                $(this).addClass("h1c")
            }
        }
    });
    
    if(customize_page){
        // love how tumblr doesn't like my on_main variable for this lol
        // normally I'd put this under if(on_main)
        // but now I have to do an ifElse like a fucking animal
    } else {
        $(".cclast").each(function(){
            if($(this).prev(".boooob").length){
                $(this).css("padding-top",0)
            }
            
            var lastP = $(this).find(".comment p:last");
            
            if(lastP.next().length == 0){
                lastP.css("margin-bottom","0")
            }
        });
        
        $(".body[original]").each(function(){
            var lastP = $(this).find("p:last");
            
            if(lastP.next().length == 0){
                lastP.css("margin-bottom","0")
            }
        })
    }
    
    if(on_main){
        $("ol.notes .original_post").each(function(){
            var getop = $(this).find("span[data-post-url]").attr("data-post-url");
            $(this).find("a.tumblelog").eq(0).attr("href",getop);

            $(this).find("blockquote").remove();
        });
        
        $("ol.notes a[rel='nofollow'][title]").removeAttr("title");
        
        $("ol.notes").each(function(){
            if(!$(this).find(".more_notes_link_container").length){
                $(this).find("li:last").css("margin-bottom","2px")
            }
        })
    }
    
    if($(".posts").eq(0).is("[reblog-borders]")){
        var bset = $(".posts").eq(0).attr("reblog-borders");
        $(".postscont").attr("reblog-borders",bset);
        $(".posts").removeAttr("reblog-borders");
    }
    
    // add space between npf image and the next reblog comment
    // if: post borders
    $(".npf_inst").each(function(){
        if($(this).parents(".comment_container").next().length){
            if($(this).next().length == 0){
                $(this).css("margin-bottom","calc((var(--Captions-Gap) / 2) + 2px)")
            }
        }
    });
    
    $(".response").each(function(){
        $(this).appendTo($(this).prev()).wrapInner("<b></b>");
        $(this).children().unwrap();
    });
    
    // remove 'deactivated' string from reblogger
    $(".commenter a").each(function(){
        var check_d = $(this).text();
        if(check_d.indexOf("-deactivated2") > -1){
            var lastdas = check_d.lastIndexOf("-");
            var gen_name = check_d.substring(0,lastdas);
            
            if($(this).parent().is(".answerer")){
                $(this).html(gen_name + "&nbsp;answered:").attr("title","user has deactivated")
            } else {
                $(this).text(gen_name).attr("title","user has deactivated")
            }
            
        }
    });
    
    // remove 'deactivated' string from via/src
    $(".viasrc a").each(function(){
        var check_d = $(this).attr("title");
        if(check_d.indexOf("-deactivated2") > -1){
            var lastdas = check_d.lastIndexOf("-");
            var gen_name = check_d.substring(0,lastdas);
            
            $(this).attr("title",gen_name + " (deactivated)")
        }
    });
    
    $(".comment_container p").each(function(){
        if($(this).next(".npf_inst").length){
            $(this).css("margin-bottom","var(--Paragraph-Margins)")
        }
    });
    
    $("a.read_more").each(function(){
        var readmoreP = $(this).parent("p");
        readmoreP.css("margin-bottom",0);
        readmoreP.css("margin-top","calc(var(--Paragraph-Margins) * 1.5)");
        $(this).css("margin-bottom",0)
    });
    
    
    $(".ccfirst").each(function(){
        if($(this).parent(".body").prev(".title").length){
            $(this).css("margin-top",0)
        }
    });
    
    $(".postinner img[alt='image']").each(function(){
        if(!$(this).parent().is("[class]")){
            $(this).css("margin-top","2px")
        }
    });
    
    $(".body > p:first").each(function(){
        if($(this).parent().is("[original]")){
            $(this).css("margin-top",0)
        }
    });
    
    // re-assign npf phoebe yellow text color
    $(".postinner p span").each(function(){
        if($(this).attr("style") == "color: #e8d738"){
            $(this).addClass("npf_color_phoebe");
            $(this).css("color","")
        }
    });
    
    $(".tmblr-attribution").each(function(){
        $(this).insertAfter($(this).parent());
    });
    
    // make gif source attribution link look like the one on dashboard
    $(".tmblr-attribution a").each(function(){
        var gifOP = $(this).text().split(" ").pop();
        
        if($(this).is("[data-peepr]")){
            var postID = $(this).attr("data-peepr").split(":").pop().slice(1).slice(0,-2);
            $(this).attr("href","//" + gifOP + ".tumblr.com/post/" + postID)
        }
        
        $(this).html("GIF by <b>" + gifOP + "</b>")
    });
    
    if($(".postscont").is("[reblog-borders*='rb-yes']")){
        $(".comment p:last").each(function(){
            if($(this).parents(".comment_container").next(".comment_container").length){
                $(this).css("margin-bottom","calc(var(--Paragraph-Margins) / 2")
            }
        })
    }
    
    // replace curly quotes with straight ones
    $("p, span, pre, code, b, i").each(function(){
        var juice = $(this).html();
        var j = juice.replaceAll('"','"').replaceAll('"','"').replaceAll("'","'").replaceAll("'","'");
        $(this).html(j)
    })
    
    /*------------------------------------------*/
    
    // readjust asker padding-top depending on text height
    $(".askright").each(function(){
        var that = this; // store the current .askright
        
        var askheight = 0;
        
        var lol;
        
        lol = setInterval(function(){
            if (askheight == "0") {
                var hassan = $(that).height();
                $(that).attr("h",hassan);
                
                var askimgH = $(that).prev(".askimg").height();
                $(that).prev(".askimg").attr("h",askimgH);
                
                if(hassan <= askimgH){
                    $(that).find(".commenter").addClass('one-liner')
                }
              }
        },0);
        
        $(window).load(function(){
            clearInterval(lol);
        });
    });
    
    $(".askimg").each(function(){
        var ifanon = $(this).attr("src");
        if(ifanon.indexOf("anonymous_avatar") > 0){
            $(this).css("transition","none");
            $(this).addClass("nofilter");
            $(this).attr("src","https://static.tumblr.com/2pnwama/dGXs9fwdp/tumblr_anon_transparent.png")
        }
        
        var howround = $(this).css("border-radius");
        if(howround == "100%"){
            $(this).css("border-radius","3px")
        }
    });
    
    // minimal soundcloud player Â© shythemes.tumblr
    // shythemes.tumblr.com/post/114792480648
    var color = getComputedStyle(document.documentElement)
               .getPropertyValue("--Link");
    
    $('.soundcloud_audio_player').each(function(){
       $(this).attr({ src: $(this).attr('src').split('&')[0] + '&amp;liking=false&amp;sharing=false&amp;auto_play=false&amp;show_comments=false&amp;continuous_play=false&amp;buying=false&amp;show_playcount=false&amp;show_artwork=true&amp;origin=tumblr&amp;color=' + color.split('#')[1], height: 116, width: '100%' });
       $(this).wrap("<div class='is_soundcloud'>")
    });
    
    /*----- UNSTRETCH PHOTOSET HEIGHTS IN CUSTOMIZATION PANEL -----*/
    if(customize_page){
        function ggw(){
            $('.photoset-grid div').each(function(){
                var thiswidth = $(this).outerWidth();
                $(this).attr("owidth",thiswidth)
            });
        }
        
        ggw();
        
        $('.photoset-grid div').each(function(){
            var steve = Date.now();
            var that = this;
            
            var int = setInterval(function(){
                if(Date.now() - steve > 3000){
                  clearInterval(int)
                } else {
                    if($(that).attr("owidth") == "0"){
                      ggw();
                  } else {
                      // when photoset image widths have loaded
                      var dw = $(that).attr("data-width");
                      var scale_phos = dw / $(that).attr("owidth");
                      $(that).attr("scale",scale_phos);

                      // apply scale to height
                      var dh = $(that).attr("data-height");
                      var waah = dh / scale_phos;
                      $(that).height(waah);
                  }
                }
                
            },0)
        });
    }
  
    // minimize whitespace on 'not found' page
    $(".postinner").each(function(){
        if(!$(this).next().length){
            $(this).css("margin-bottom","3px")
        }
      
        if($(this).next(".tagcont").length){
            if($(this).parent(".posts").is("[on-permapage]")){
              $(this).css("margin-bottom","var(--Captions-Gap)")
            }
        }
    });

    $(".body b, .body span, .comment b, .comment span").each(function(){
        var thistext = $(this).text();
        var regExp = /[a-zA-Z]/g;
      
        if(regExp.test(thistext)){
          if(thistext === thistext.toUpperCase()){
              $(this).addClass("allcaps")
          }
        }
    });
    
    /*--------- BLACK OR WHITE TUMBLR CONTROLS ---------*/
    if(on_main){
        var tc = getComputedStyle(document.documentElement)
                .getPropertyValue("--TumblrControls-Color");
        
        var nao = Date.now();
        
        var trex = setInterval(function(){
            if(Date.now - nao > 6969){
                clearInterval(trex)
            } else {
                if(tc == "black"){
                    $(".iframe-controls--desktop").addClass("tc-black");
                }
                
                $(".iframe-controls--desktop").css("visibility","visible");
                $(".iframe-controls--desktop").addClass("t-opac");
            }
        },0);
    }
    
    /*------- CUSTOMIZE PAGE ADJUSTMENTS -------*/
    if(customize_page){
        $(".nc-word").each(function(){
            $(this).css("margin-top",".5px")
        });
        
        $(".tumblr_blog").each(function(){
            $(this).appendTo($(this).parent("p").parent(".comment").prev(".commenter"));
            $(this).prev("a:not([class])").remove();
        });
      
        $(".comment p").each(function(){
            $(this).contents().filter(function(){
                return this.nodeType === 3;
            }).wrap('<span/>');
        })
        
        $(".posts .comment p span, .posts .comment p").each(function(){
            var count = $(this).text().length;
            if(count == "1"){
                if($(this).prev().is(".tumblr_blog")){
                    // do nothing
                } else {
                    $(this).remove();
                }
            }
        });      
        
      
        $(".comment blockquote").each(function(){
            if($(this).prev().length == 0){
                $(this).wrapInner("<div class='kek'>");
            }
        });
      
        $(".kek").each(function(){
            $(this).unwrap();
            $(this).children().unwrap();
        });
      
        $(".npf_col").each(function(){
            $(this).parents(".npf_row").next("p").css("margin-top",0)
        })
      
        /*----------------------------*/
        // make reblog order/format and reblog icons more accurate
        $(".tumblr_blog").each(function(){
            if($(this).prev().is("img")){
                var blogname = $(this).text();
                
                $(this).prev().attr("src","//api.tumblr.com/v2/blog/" + blogname + ".tumblr.com/avatar/64")
            }
            
            if($(this).next().is("span")){
                $(this).parent("p").remove()
            }
        });
        /*----------------------------*/
        
        $(".commenter img").each(function(){
            if($(this).next("a").is(":empty")){
                $(this).parent().remove()
            }
        });
        
        $(".npf_inst.boooob").each(function(){
            if($(this).next().is(".postinner") || $(this).next().is(".body")){
                $(this).css("margin-bottom","calc(var(--NPF-Bottom-Gap-From-Captions) * 1.369)")
            }
            
            if($(this).next().is(".comment_container")){
                $(this).next().css("padding-top",0)
            }
        });
        
        $(".posts blockquote").each(function(){
            if($(this).prev().is("p")){
                $(this).children().unwrap();
            }
        });
        
        $(".npf_inst:not(.boooob)").each(function(){
            if($(this).prev().is("p")){
                $(this).css("margin-top","var(--NPF-Bottom-Gap-From-Captions)")
            }
            
            if($(this).closest(".comment").length){
                $(this).css("margin-top",0)
            }
        });
        
        $(".npf_inst").each(function(){
            if($(this).next().is(".postinner")){
                $(this).css("margin-bottom","calc(var(--NPF-Bottom-Gap-From-Captions) * 1.369)")
            }
        })
        
        // detect duplicate reblogs and remove the dupes
        $(".comment_container").each(function(){
            var perma = $(this).find("a.tumblr_blog").eq(0).attr("href");
            $(this).attr("perma",perma);
        });
        
        $("[perma]").each(function(){
            if($(this).attr("perma") == $(this).next().attr("perma")){
                $(this).next().hide().addClass("donewiththisshit")
            }
        });
        
        $(".donewiththisshit").each(function(){
            $(this).remove();
        });
        
        $(".comment_container span").each(function(){
            if($(this).text() == "(Source: "){
                $(this).parent("p").hide();
            }
        });
        
        $(".tin").each(function(){
            $(this).css("margin-left","var(--Captions-Gap)")
        });
        
        $(".postinner > .chatholder").each(function(){
            $(this).css("margin-top",0)
        })
        
        $(".heart").each(function(){
            $(this).parent().attr("title","unable to like post from customization panel! exit to main blog to try it")
        });
        
        /*
        $(".user-symbol").each(function(){
            if($(this).next("a").attr("href") == ""){
                $(this).parent(".commenter").remove();
            }
        })
        */
        
        if($(".npf_inst").eq(0).height() == "0"){
            // location.reload();
        }
        
        if(!$(".photoset-grid div").eq(0).is("[owidth]")){
            // location.reload();
        }
    }// end customizepage adjustments
    
    // clicktags
    $(".viewtags").click(function(){
        var le_tags = $(this).parents(".permarow").prev(".pho").find(".tagcont");
        var that = this;
        
        if($(this).is("[tags-opt='yesclick']")){
            le_tags.slideToggle(400);
        } else {
            // toggle tags with fade
            if(!$(this).is("[clicked]")){
                le_tags.slideDown(400);
                
                setTimeout(function(){
                    le_tags.addClass("tagsfade");
                    $(that).attr("clicked","");
                },300);
                
            } else {
                le_tags.removeClass("tagsfade");
                
                setTimeout(function(){
                    le_tags.slideUp(400);
                    $(that).removeAttr("clicked");
                },400)
            }
        }
    });
    
    // find out-of-bounds npf photo(s) and put them back inside the posts
    var dn = Date.now();
    var po = setInterval(function(){
      if(Date.now() - dn > 5000){
        clearInterval(po);
      } else {
        if($(".photo_origin").length){
          rel();
        }
      }
    },0);
    
    function rel(){
      $(".photo_origin").each(function(){
          if($(this).prev().length == 0){

            if($(this).parent().is(".textpost")){
              $(this).prependTo($(this).parent().find(".comment_container").eq(0))
              $(this).parent(".comment_container").css("margin-top",0)
            }
          }
      })
    }
    
    // attempt to fix the weird black line that appears at bottom of posts
    function res_posts(){
        $(".postinner").each(function(){
            var heh = $(this).height();
            $(this).height(Number(heh) + Number(0.5));
        })
    }
    
    if(on_main){
        setTimeout(function(){
            res_posts();
			$(".postinner").css("height","")
        },1000);
    }
    
    /*---- SIDEBAR STUFF ----*/
    
    // sidebar 1 desc height - apply to animation
    $(".sbd").each(function(){
        if($(this).attr("vis") == "hover"){
            var sbh = $(this).height();
            $(this).addClass("repose");
            $(this).parent(".turnip").css("margin-bottom",-sbh + "px");
        }
        
        if($(this).attr("vis") == "always-show"){
            $(this).addClass("repose");
            $(this).prev().find(".sidebar-title").addClass("beer")
        }
    })
    
    // add crd
    $(".tortilla").each(function(){
        $(this).prepend("<div class='pesto'></div>");
    })
    
    $(".pesto").each(function(){
        $(this).prepend("<a title='' href='//glenthemes.tumblr.com'>theme by glenthemes</a>");
        $(this).find("a").removeAttr("title")
    })
    
    $(".avi-title").each(function(){
        var that = this;
        var t_t = $.trim($(this).html());
        
        if (t_t.match(/[\u3400-\u9FBF]/)){
            $(this).addClass('jpn');
            $(this).wrap("<div></div>");
            $(this).after("<div class='charge'>&#xB7;&#xB7;&#xB7;</div>");
            $(this).css("height","0");
            
            window.onload = function(){
                $(that).next(".charge").remove();
                $(that).css("height","");
                $(that).css("opacity","1");
                
                var twt = $(that).width();
                var tht = twt/7.7;
                
                var baaah = twt/10;
                
                twt = twt + baaah*1.5;
                tht = tht + baaah*1.5;
                
                
                $(that).find(".undyne").width(twt)
                $(that).find(".undyne").height(tht);
                
                $(that).find(".undyne").css("margin-bottom",-tht);
                $(that).find(".undyne").css("margin-left",-baaah);
            }
        } else {
            $(this).css("opacity","1");
            var twt = $(this).width();
            var tht = twt/7.7;
            
            var baaah = twt/10;
            
            twt = twt + baaah*1.5;
            tht = tht + baaah*1.5;
            
            
            $(this).find(".undyne").width(twt)
            $(this).find(".undyne").height(tht);
            
            $(this).find(".undyne").css("margin-bottom",-tht);
            $(this).find(".undyne").css("margin-left",-baaah);
        }
    })
    
    $(".descbox p:last").each(function(){
        if(!$(this).next().length){
            $(this).css("margin-bottom",0)
        }
    })
    
    /* full-fit post width */
    function fullPostWidth(){
        if($("[post-width]").attr("post-width") == "fill"){
            var sb1w = $(".leftside").width(); // sidebar 1 width
            var sb2w = $(".sidebeer").width(); // sidebar 2 width
            var marge = parseInt($(".postscont").css("margin-top"));
            var scb = parseInt(getComputedStyle(document.documentElement).getPropertyValue("--Scrollbar-Padding"));
            var pod = parseInt($(".posts").eq(0).css("padding-top"));
            
            var bonk = $(window).width() - sb1w - sb2w - marge*2 - pod*2 + scb/2;
            
            $(".posts").css("width",bonk);
            $(".notescont").css("width",bonk);
            
            if(customize_page){
                if($(".posts").width() < 150){
                     $(".posts").width(500)
                }
            }
        }
    }
    
    fullPostWidth();
	
	setTimeout(function(){
		fullPostWidth()
	},2500);
    
    window.onresize = function(){
        fullPostWidth();
        $(".postinner").addClass("suce")
    }
    
    /*--- load the message thing for user ---*/
    if(customize_page){
        $("body").append("<div class='moo'><div class='squeesh'>hi there! some things may look wonky whilst you're still in customize mode. view your blog in a new tab to check your changes!</div></div>");
        $(".squeesh").click(function(){
            $(this).parent().slideToggle()
        })
    }
    
    /*--- music player stuff ---*/
    var aaa = document.getElementById("musique");

    if($("#musique").is("[autoplay]")){
        $(".pausee").addClass("aff");
        $(".playy").addClass("beff");
    }
        
    $(".music-controls").click(function(){
        if (aaa.paused) {
          aaa.play();
          $(".pausee").toggleClass("aff");
          $(".playy").toggleClass("beff");
        } else {
          aaa.pause();
          $(".playy").toggleClass("beff");
          $(".pausee").toggleClass("aff");
        }
    });
    
    aaa.onended = function(){
        $(".playy").removeClass("beff");
    	$(".pausee").removeClass("aff");
    };
    
    $("#musique").each(function(){
        var mp3 = $.trim($(this).attr("src"));
        mp3 = mp3.replaceAll("?dl=0","").replaceAll("www.dropbox","dl.dropbox");
        $(this).attr("src",mp3)
    })
	
	// tooltips
	$("a[title]").style_my_tooltips({
        tip_follows_cursor:true,
        tip_delay_time:0,
        tip_fade_speed:69,
        attribute:"title"
    });
	
	// check if user doesn't want a sidebar icon
	// if no-image --> set user's icon
	$(".avi-icon").each(function(){
		var gsrc = $(this).attr("src");
		if(gsrc.indexOf("assets.tumblr.com/images/x.gif") > -1){
			var useravi = $("[user-avi]").text();
			$(this).attr("src",useravi);
		}
	});
    
});//end ready
